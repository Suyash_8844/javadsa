import java.util.*;
class HashMapDemo1{
	public static void main(String[] args){

		HashMap hm=new HashMap();
		hm.put("Java",".java");
		hm.put("python",".py");
		hm.put("Dart",".dart");

		System.out.println(hm);

		//get

		System.out.println(hm.get("python"));

		//Keyset
		
		System.out.println(hm.keySet());

		//Values

		System.out.println(hm.values());

		//EntrySet
		
		System.out.println(hm.entrySet());

	}
}
