import java.util.*;
class SortDemo{

	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add("Kanha");
		al.add("Ashish");
		al.add("Badhe");
		al.add("Shashi");
		al.add("Rahul");

        	System.out.println(al);  //[Kanha,Ashish,Badhe,Shashi,Rahul]
	
		TreeSet ts = new TreeSet();
		System.out.println(ts);  //[Ashish,Badhe,Kanha,Rahul,Shahi]
                                  
		Collections.sort(al);    
		System.out.println(al);
	}
}
