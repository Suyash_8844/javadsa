import java.util.*;

class SortedMapDemo{
	public static void main(String[] args){

		SortedMap tm= new TreeMap();

		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("SL","SriLanka");
		tm.put("Aus","Australia");
		tm.put("Ban","Bangladesh");

		System.out.println(tm);

		// SubMap

		System.out.println(tm.subMap("Aus","Pak"));

		// headMap
		
		System.out.println(tm.headMap("Pak"));

		// firstkey
		
		System.out.println(tm.firstKey());

		// Lastkey
		
		System.out.println(tm.lastKey());


		// keySet
		
		System.out.println(tm.keySet());

		// Values 
		
		System.out.println(tm.values());

		// entrySet

		System.out.println(tm.entrySet());
	}
}
