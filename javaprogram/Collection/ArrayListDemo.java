import java.util.*;

class ArrayListDemo{
	public static void main(String[] args){

		ArrayList al=new ArrayList();

		// add(Element);

		al.add(10);
		al.add(20.5f);
		al.add("Suyash");
	        System.out.println(al);

		// int size();

		System.out.println(al.size());
 
		// boolean containsObject);
		
		System.out.println(al.contains("Suyash")); //true
                System.out.println(al.contains(20)); //false
                 
                // int indexOf(Object);
		
                System.out.println(al.indexOf(20.5f));

                // int lastIndexOf(Object);
		
	        System.out.println(al.lastIndexOf(20.5f));

	        // E get(int);

	        System.out.println(al.get(2));

                // E set(int, E);

                System.out.println(al.set(2,"Incubater"));	  
	        System.out.println(al);

	        // void add(int ,E);

	        al.add(3,"Core2Web");
	        System.out.println(al);

	        // E remove(int);
		
	        System.out.println(al.remove(3));
	        System.out.println(al);
               
		// boolean addAll(Collection);
		
		ArrayList al2=new ArrayList();
		al.addAll(al);
		System.out.println(al);
	
	        // boolean addAll(int, Collection);
		
	        al.addAll(3,al2);
	        System.out.println(al);

	        // java.lang.Object[] to Array();
		
		Object arr[]=al.toArray();
		for(Object data:arr){
			System.out.println(data+" ");
		}
		System.out.println();
		


	}
}
