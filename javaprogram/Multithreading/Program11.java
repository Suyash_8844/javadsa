class MyThread extends Thread{
	MyThread(){

		super();

	}

	MyThread(String str){

		super(str);

	}
	public void run(){

		System.out.println(getName());
	}
}
class ThreadGroupDemo{

	public static void main(String[] args){
		MyThread obj1=new MyThread("Thread-1");
		obj1.start();

		MyThread obj2=new MyThread("Thread-2");
		obj2.start();

		MyThread obj3=new MyThread("Thread-3");
		obj3.start();

		MyThread obj=new MyThread();
		obj.start();

		System.out.println(Thread.currentThread().getName());

	}
}
