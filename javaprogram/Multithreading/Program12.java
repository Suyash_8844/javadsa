//Creating Parent thread and child thread.
class MyThread extends Thread{

	MyThread(ThreadGroup tg,String str){

		super(tg,str);

	}
	public void run(){

		System.out.println(Thread.currentThread());

	}
}
class ThreadGroupDemo{
	public static void main(String[] args){

		ThreadGroup pthreadGP=new ThreadGroup("Core2Web");

		MyThread obj1=new MyThread(pthreadGP,"c");
		MyThread obj2=new MyThread(pthreadGP,"java");
		MyThread obj3=new MyThread(pthreadGP,"python");

		obj1.start();
                obj2.start();
                obj3.start();

		ThreadGroup cthreadGP=new ThreadGroup(pthreadGP,"Incubater");

                MyThread obj4=new MyThread(cthreadGP,"Flutter");
                MyThread obj5=new MyThread(cthreadGP,"ReactJs");
                MyThread obj6=new MyThread(cthreadGP,"Springboot");

		obj4.start();
		obj5.start();
		obj6.start();
	}
}
