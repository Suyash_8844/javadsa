/* Given an integer array of size N
   count the no of element having atleast 1 element greater than itself

   Arr:[2,5,1,4,8,0,8,1,3,8]
   N:10

   Output:7

   Optimized approach
*/

class ArrayDemo{
        public static void main(String[] args){
		int arr[]={2,5,1,4,8,0,8,1,3,8};
		int N=10;
		int count=0;
		int maxEle=Integer.MIN_VALUE;
		// max
		 
		for(int i=0;i<N;i++){
			if(arr[i]>maxEle)
				maxEle=arr[i];
		}

		//count

		for(int i=0;i<N;i++){
			if(arr[i]==maxEle)
				count++;
		}
		System.out.println(N-count);
	}
}


