//  WAP in which students will enter marks of 5 Different subjects.If all subject having 
//  above passing marks add them and provide to switchcase  to print grades
import java.util.*;

class SwitchDemo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		
		int marathiMarks;
		System.out.print("Enter Marathi Marks = ");
		marathiMarks=sc.nextInt();

		int EngMarks;
		System.out.print("Enter Eng Marks = ");
		EngMarks=sc.nextInt();
		
		int mathsMarks;
		System.out.print("Enter Maths Marks = ");
		mathsMarks=sc.nextInt();
		
		int socialSciMarks;
		System.out.print("Enter Social Science Marks = ");
		socialSciMarks=sc.nextInt();
		
		int SciMarks;
		System.out.print("Enter Science Marks = ");
		SciMarks=sc.nextInt();

		if(marathiMarks<35 || EngMarks<35 || mathsMarks<35 || socialSciMarks<35 || SciMarks<35){
			System.out.println("You Failed Exam");
		}else{
			double avg=(marathiMarks+EngMarks+mathsMarks+socialSciMarks+SciMarks)/5;

			char ch;
			if(avg>75)
				ch='A';

			else if(avg>60)
				ch='B';

			else if(avg>50)
				ch='C';

			else if(avg>40)
				ch='D';

			else if(avg>35)
				ch='E';
			else
				ch='F';

			switch(ch){
				case 'A':
						System.out.println("First class with distinction");
						break;

				case 'B':
						System.out.println("First class");
						break;

				case 'C':
						System.out.println("Second class");
						break;
				
				case 'D':
						System.out.println("Third class");
						break;
						
				case 'E':
						System.out.println("Pass");
						break;
				
				case 'F':
						System.out.println("Fail");
						break;
			}
		}
	}
}

						

