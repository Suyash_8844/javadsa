// WAP in which user should enter two numbers if both numbers are positive
// multiply them and provide to switch case to verify
import java.util.*;

class SwitchDemo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		int num1,num2;

		System.out.print("Enter Number 1 = ");
		num1=sc.nextInt();

		System.out.print("Enter Number 2 = ");
		num2=sc.nextInt();

		if(num1<0 || num2<0){
			System.out.println("Negative Numbers are not allowed");
		}else{
			switch((num1*num2)%2){
				
				case 0:
					System.out.println("The Multiplication is even");
					break;
				
				case 1:
					System.out.println("The Multiplication is odd");
					break;
			}
		}
	}
}
