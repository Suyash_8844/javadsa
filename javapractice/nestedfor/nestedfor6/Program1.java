// WAP to print 1 to 50 divisible 5 and 2 and its count
import java.io.*;

class Program1{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Start And End");
		int start=Integer.parseInt(br.readLine());
		int end=Integer.parseInt(br.readLine());

		int count=0;
		for(int i=start;i<=end;i++){
			if(i%5==0 && i%2==0){
				count++;
				System.out.println(i+"\t");
			}
		}
		System.out.println("Count = "+count);
	}
}
		 

