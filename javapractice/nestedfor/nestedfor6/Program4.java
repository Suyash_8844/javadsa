// WAP to print perfect cubes form the range
import java.io.*;

class Program4{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Start And end");

		int start=Integer.parseInt(br.readLine());
		int end=Integer.parseInt(br.readLine());

		for(int i=start;i<=end;i++){
			for(int j=1;j*j*j<=i;j++){
				if(j*j*j==i){
					System.out.println(i+"\t");
				}
			}
		}
	}
}


