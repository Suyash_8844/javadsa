// WAP to print all even numbeers in reverse order and odd numbers in standard way
import java.io.*;

class Program4{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER START AND END");
		int start=Integer.parseInt(br.readLine());
		int end=Integer.parseInt(br.readLine());

		System.out.println("Even Numbers");

		for(int i=end;i>=start;i--){
			if(i%2==0){
				System.out.print(i+" ");
			}
		}
		System.out.println();

		System.out.println("Odd NUMBERS");
		for(int i=start;i<=end;i++){
			if(i%2==1){
				System.out.print(i+" ");
			}
		}
		System.out.println();
	}
}


