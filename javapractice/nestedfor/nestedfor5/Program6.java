// WAP and take two characters if these characters are equal then prinbt them as it is
// but if they are unequal print their difference
import java.io.*;

class Program6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter 2 characters");
		char ch1=(char)br.read();
		br.skip(1);
		char ch2=(char)br.read();

		if(ch1==ch2){
			System.out.println("The 2 Characters are same");
		}else{
			int diff=ch1-ch2;
			if(diff<0){
				diff=diff*(-1);
			}
			System.out.println("the difference betwn "+ch1+" and "+ch2+" characters is "+diff);
		}
	}
}


