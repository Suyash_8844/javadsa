// 5 4 3 2 1
// 8 6 4 2
// 9 6 3
// 8 4
// 5
import java.io.*;

class Program3{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter ROws");
		int rows=Integer.parseInt(br.readLine());
		for(int i=1;i<=rows;i++){
			int num=rows-i+1;
			for(int j=rows;j>=i;j--){
				System.out.print(num*i+"\t");
				num--;
			}
			System.out.println();
		}
	}
}

