// O
// 14 13
// L  K  J
// 9  8  7  6
// E  D  C  B  A
import java.util.*;

class Program7{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Rows");
		int row=sc.nextInt();
		int x=(row*(row+1))/2;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if((i%2==0 && row%2==0) || (i%2==1 && row%2==1)){
					System.out.print((char)(64+x)+" ");
				}else{
					System.out.print(x+" ");
				}
				x--;
			}
			System.out.println();
		}
	}
}

