// WAP to take a number as input and print addition of factorials of each digit
import java.util.*;

class Program9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int number=sc.nextInt();
		int num=number;
		int x=0;
		while(num!=0){
			int rem=num%10;
			int prod=1;
			while(rem!=0){
				prod=prod*rem;
				rem--;
			}
			x=x+prod;
			num=num/10;
		}
		System.out.println("addition of factorial of each digit from "+number+"= " +x);
	}
}
		     

